package nonaccessmodifiers.statickeyword;

public class OverridingStaticMethod {


}


class ClassWithStaticMethodAndVariable{
    static String description = "Its description variable from " + ClassWithStaticMethodAndVariable.class;

    static void showDescription(){
        System.out.println(description);
    }

    void nonStaticmethod(){
        System.out.println("Its non static method in " + ClassWithStaticMethodAndVariable.class);
    }
}

class SimpleClass extends ClassWithStaticMethodAndVariable{

    static String description = "Its description variable from " + SimpleClass.class;

    // you can define method with same signature in base class but it not overrite it
   // when use polymorphism this method is not called
    static void showDescription(){
        System.out.println(description);
    }

    @Override
    void nonStaticmethod() {
        System.out.println("Its non static method in " + SimpleClass.class);
    }
}


 class ExecutableOverrideStaticMethod {

     public static void main(String[] args) {
         ClassWithStaticMethodAndVariable obj1 = new ClassWithStaticMethodAndVariable();
         obj1.showDescription(); //Its description variable from class nonaccessmodifiers.statickeyword.ClassWithStaticMethodAndVariable
         obj1.nonStaticmethod(); //Its non static method in class nonaccessmodifiers.statickeyword.ClassWithStaticMethodAndVariable


         SimpleClass obj2 = new SimpleClass();
         obj2.showDescription(); // Its description variable from class nonaccessmodifiers.statickeyword.SimpleClass
         obj2.nonStaticmethod(); // Its non static method in class nonaccessmodifiers.statickeyword.SimpleClass


         ClassWithStaticMethodAndVariable obj3 = new SimpleClass();
         // keep in mind you can declare static method with a signature from base class. but its not overriding.
         obj3.showDescription(); // Its description variable from class nonaccessmodifiers.statickeyword.ClassWithStaticMethodAndVariable
         // non static method called that reside in Simple Class but static method called from base class
         obj3.nonStaticmethod(); // Its non static method in class nonaccessmodifiers.statickeyword.SimpleClass
         // its called re-declared method inside SimpleClass
         SimpleClass.showDescription(); //Its description variable from class nonaccessmodifiers.statickeyword.SimpleClass
     }

    }