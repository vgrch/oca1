package nonaccessmodifiers.statickeyword;

public class SimpleClassWithStaticMembers {


    // static and final combination defines constants
    private static final int MAX_INSTANCE_COUNT = 10;


    // its instance variable, unique for each instance
    String description;

    // when variable defined with static non accesss modifiers it means this field belong class not instance
    static int instance_count;


    // every time instance created from this class instance_count will increased 1 step
    //  make constructor private to prevent create instance from another class
    private SimpleClassWithStaticMembers() {
        instance_count++;
    }

    public static SimpleClassWithStaticMembers getInstance() {
        if (instance_count < MAX_INSTANCE_COUNT) {
            System.out.println("Creating new instance. Available instance count : " + instance_count);
            return new SimpleClassWithStaticMembers();
        }
        else{
            System.out.println("Instance count exceed max instance count cannot create new instance ");
            return null;
        }
    }


}


class Executable {
    public static void main(String[] args) {
        System.out.println("Creating first instance");
        SimpleClassWithStaticMembers instance1 = SimpleClassWithStaticMembers.getInstance();
        // define description for first instance its indiviual for this instance
        instance1.description = "its instance 1";
        // get instance count will be 1
        System.out.println("Instance count of class : " + SimpleClassWithStaticMembers.class.getName() + " is : " + SimpleClassWithStaticMembers.instance_count);
        System.out.println(instance1.description);
        System.out.println(instance1.instance_count);

        System.out.println("Creating second instance");
        SimpleClassWithStaticMembers instance2 = SimpleClassWithStaticMembers.getInstance();
        // define description for second instance its indiviual for this instance
        instance1.description = "its instance 2";
        // get instance count will be 2
        System.out.println("Instance count of class : " + SimpleClassWithStaticMembers.class.getName() + " is : " + SimpleClassWithStaticMembers.instance_count);
        System.out.println(instance1.description);
        System.out.println(instance2.instance_count);

        // currently there are 2 instance of class SimpleClassWithStaticMembers
        // this loop create new instances while instance count reach MAX_INSTANCE_COUNT variable in SimpleClassWithStaticMembers class
        for (int i = 0; i < 10; i++) {
            SimpleClassWithStaticMembers.getInstance();
        }

        // you can access static variable using null reference variable

        SimpleClassWithStaticMembers nullreference = null;
        System.out.println(nullreference.instance_count);
    }
}


class TestAccessStaticMembersFromNonStaticMembers{

    // class variable
    static int a;

    // instance variable
    int b;

    //static method
    static void methodA(){
        System.out.println(a);
        // static members could not access non static members
       // System.out.println(b);
    }

    void methodB(){
        // non static members can access static members
        System.out.println(a);
        System.out.println(b);
    }


}