package nonaccessmodifiers.finalkeyword;

// final class cannot extend by another class
public final class SimpleFinalClass {
    public final String description;

    {
        description = "Final variable can assign value only once";
    }


    // if you try reassign final variable you faced compile error
//    public void reassign(){
//        description = "";
//    }
}

class SimpleNonFinalClassWithFinalMethod {
    public final String description;

    {
        description = "Final variable can assign value only once";
    }

    public final void simpleFinalMethod() {

    }
}

class SimpleNonFinalClass extends SimpleNonFinalClassWithFinalMethod {
    // if you try override final method in base class you will faced compile error
    // public final void simpleFinalMethod(){}

    {
        // final variable inherited by derived class
        System.out.println(description);
    }

    public static void main(String[] args) {
        SimpleNonFinalClass nonFinalClass = new SimpleNonFinalClass();
    }

}

// will cause compile error cannot inherit from final class
// final class AnotherSimpleFinalClass extends SimpleFinalClass{}

// will cause compile error cannot inherit from final class
// class SimpleNonFinalClass extends SimpleFinalClass{}


// final interface. you cannot define interface as final . because by default interfaces are abstract
// final interface FinalInterface {}