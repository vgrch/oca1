package nonaccessmodifiers.abstractkeyword;

// abstract keyword when used class declaration this class cannot be instantiate
// you could not define abstract class final
public abstract class SimpleAbstractClassWithAbstractMethods {

    // you could not define abstract variable
    // public abstract int test;

    // abstract method type of java method which does not obtain body
    // if any concrete class extends this class should override abstract methods
    public abstract void methodWithoutBody();

    // you cannot use abstract non access modifier with static and final keyword
    // final abstract void finalMethodWithoutBody();
    // public static abstract staticMethodWitoutBody();
    // private abstract void privateMethodWithoutBody();
    protected abstract void protectedMethodWithoutBody();
    abstract void packageLevelMethodWithoutBody(); // default package level


    //abstract method cannot have body
    // it's not abstract method because curly brackets
    // public abstract void abstractMethodWithBody(){}

    public static void main(String[] args) {
        SimpleAbstractClassWithAbstractMethods object;
        // object = new SimpleAbstractClassWithAbstractMethods(); // will cause compile error
    }
}


abstract class SimpleAbstractClassExtendsAbstractClass extends SimpleAbstractClassWithAbstractMethods {

}

// if concrete class extends abstract class with abstract methods, this must override all abstract method
class SimpleConcreteClass extends SimpleAbstractClassWithAbstractMethods{

    @Override
    public void methodWithoutBody() {

    }

    @Override
    protected void protectedMethodWithoutBody() {

    }

    @Override
    void packageLevelMethodWithoutBody() {

    }
}

// by default all interfaces are abstract
interface SimpleInterface {

}

abstract interface SimpleInterface1 {

    abstract void test();// abstract keyword is reduntant here

    // by default interface method without body marked as abstract
    void test1();

}