package nonaccessmodifiers.abstractkeyword;

// you can define class as abstract even there is not any abstract method
public abstract class AbstractClassWithoutAbstractMethod {

    public void nonAbstractMethod() {
        System.out.println("This method has body");
    }

}
