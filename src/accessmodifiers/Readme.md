Access modifiers control the accessibility of a class or an interface, including its mem bers (methods and variables), 
by other classes and interfaces within the same or separate packages.


Access modifiers can be applied to classes, interfaces, and their members - instance and class variables and methods.


Local variables and method parameters can’t be definedusing access modifiers.

Java has four access levels:
- public 
- protected
- default
- private

But three access modifiers. There is not any explicitly defined keyword for default:
- public 
- protected
- private

###Java entities and access modifiers

| Entity name                   | public        | Protected     | Private       |
| :---:                         |    :----:     |          :---:|        :---:  |
| Top level class,inteface      | OK            | Nope          | NOPE          |
| Class members(static)         | OK            | OK            | OK            |
| Instance members              | OK            | OK            | OK            |
| Method params and local var   | NOPE          | NOPE          | NOPE          |

