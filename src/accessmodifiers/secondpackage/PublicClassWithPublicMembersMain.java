package accessmodifiers.secondpackage;

import accessmodifiers.firstpackage.PublicClassWithPublicMembers;

public class PublicClassWithPublicMembersMain {

    public static void main(String[] args) {
        PublicClassWithPublicMembers withPublicMembers = new PublicClassWithPublicMembers();

        withPublicMembers.description = "Defined from another class in different package";
        withPublicMembers.showDescription();
    }

}
