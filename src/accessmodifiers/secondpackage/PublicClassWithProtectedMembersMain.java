package accessmodifiers.secondpackage;

import accessmodifiers.firstpackage.PublicClassWithProtectedMembers;
import accessmodifiers.firstpackage.PublicClassWithPublicMembers;

public class PublicClassWithProtectedMembersMain {


    public static void main(String[] args) {
        PublicClassWithProtectedMembers object = new PublicClassWithProtectedMembers();
        // object.description; // will cause compile error
        // protected members is not accessible from another package by creating object.
    }
}
