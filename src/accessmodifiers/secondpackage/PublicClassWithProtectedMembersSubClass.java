package accessmodifiers.secondpackage;

import accessmodifiers.firstpackage.PublicClassWithProtectedMembers;

public class PublicClassWithProtectedMembersSubClass extends PublicClassWithProtectedMembers {

    public void test(){
        description = "protectes members are accessible by inheritance";
        showDescription();
    }

    public static void main(String[] args) {
        PublicClassWithProtectedMembersSubClass object = new PublicClassWithProtectedMembersSubClass();
        object.test();

        PublicClassWithProtectedMembers object1 = new PublicClassWithProtectedMembers();
//        object1.description = "Protected members are not accessible another class in separate packages by using reference variable"; // will cause compile error
    }
}
