package accessmodifiers.secondpackage;

import accessmodifiers.firstpackage.PublicClassWithPrivateMembers;

public class PublicClassWithPrivateMembersMain {

    public static void main(String[] args) {
        PublicClassWithPrivateMembers object = new PublicClassWithPrivateMembers();
        // object.description="Private members is not accessible outside class"; // will cause compile error
    }

}
