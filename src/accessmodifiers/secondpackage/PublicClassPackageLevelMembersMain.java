package accessmodifiers.secondpackage;

import accessmodifiers.firstpackage.PublicClassPackageLevelMembers;

public class PublicClassPackageLevelMembersMain {

    public static void main(String[] args) {
        PublicClassPackageLevelMembers object = new PublicClassPackageLevelMembers();
       // object.description = "Will cause compile time error.Package level members only accessible classes that reside same package";
    }
}
