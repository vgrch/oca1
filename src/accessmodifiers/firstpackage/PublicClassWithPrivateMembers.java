package accessmodifiers.firstpackage;

public class PublicClassWithPrivateMembers {

    private String description;

    private void showDescription() {
        System.out.println(description);
    }

    public static void main(String[] args) {
        PublicClassWithPrivateMembers _this = new PublicClassWithPrivateMembers();
        _this.description = "We crate new instance inside class.Its acceptable;";
        _this.showDescription();
    }
}
