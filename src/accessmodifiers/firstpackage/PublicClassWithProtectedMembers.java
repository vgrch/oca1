package accessmodifiers.firstpackage;

public class PublicClassWithProtectedMembers {

    protected String description;

    protected void showDescription(){
        System.out.println(description);
    }
}
