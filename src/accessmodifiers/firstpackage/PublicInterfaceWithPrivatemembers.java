package accessmodifiers.firstpackage;

interface PublicInterfaceWithPrivatemembers {

    // java 8 not allowed private members inside interface
    //private String description;
    //private void privateMethod();

}
