package accessmodifiers.firstpackage;

public class PublicClassWithPublicMembers {

    public String description;

    public void showDescription(){
        System.out.println(description);
    }
}
