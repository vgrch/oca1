package accessmodifiers.firstpackage;

class ClassA {
    private String description;
}


class ClassB {
    private String description;
}


class ClassC {
    private String description;
}

class Main {
    final static public void main(final String... args) {
        ClassC classC = new ClassC();
        // classC.description = "Will cause compile error. even both classes reside in same java source code file";
    }
}