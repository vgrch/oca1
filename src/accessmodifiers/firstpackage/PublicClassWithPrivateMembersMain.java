package accessmodifiers.firstpackage;

public class PublicClassWithPrivateMembersMain {

    public static void main(String[] args) {
        PublicClassWithPrivateMembers object = new PublicClassWithPrivateMembers();
//        object.description="Private members is not accessible outside class even both classes reside same package"; // compile error
    }
}
