package accessmodifiers.firstpackage;

public class PublicClassPackageLevelMembersMain {

    public static void main(String[] args) {

        PublicClassPackageLevelMembers object = new PublicClassPackageLevelMembers();
        object.description="Within same package package level members accesible by other classes";
        object.showDescription();
    }
}
