package accessmodifiers.firstpackage;

public class PublicClassWithProtectedMembersMain {

    public static void main(String[] args) {
        PublicClassWithPublicMembers withPublicMembers = new PublicClassWithPublicMembers();

        // protected members accessible by another class in same package by creating object
        withPublicMembers.description = "Defined from another class in different package";
        withPublicMembers.showDescription();

    }

}
