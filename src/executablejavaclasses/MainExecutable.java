package executablejavaclasses;

//test static import

import static structureofjavasourcecodefile.structureofclass.staticimport.ClassContainsStaticMembers.*;

public class MainExecutable {
//    requirement of executable class is create method with below signature
//    The method must be marked as a public method.
//    The method must be marked as a static method.
//    The name of the method must be main.
//    The return type of this method must be void.
//    The  method  must  accept  a  method  argument  of  a String  array  or  a  variableargument (varargs) of type String
//    name of argument doesn't matter.

    // run from command line: java MainExecutable itsargument1 itsatgument2
    public static void main(final String[] args) {

        // we able use below members by import static statement
        name = "It's name variable of ClassContainsStaticMembers class";
        showName();

        // if class run from command line without any argument args parameter will be empty array not null;
        if (args.length != 0) {
            for (int i = 0; i < args.length; i++) {
                System.out.println("Class argument "+ i + " is " + args[i]);
            }
        }

        System.out.println("Executed successfully");
    }

    // all below methods acceptable as entry point

    /*
    public static void main(String args[]) {
        System.out.println("Executed successfully");
    }
    */

    /*
    static public void main(final String[] a) {
        System.out.println("Executed successfully");
    }
    */

    /*
    public final static void main(String[] arguments) {
        System.out.println("Executed successfully");
    }
    */

    /*
     static public final void main(String... test) {
        System.out.println("Executed successfully");
    }
    */

        /*
     static public final void main(String args...) {
        System.out.println("Failed. Ellipsis must follow data type");
    }
    */
}