//it is java source code file
//A Java source code file is used to define Java entities such as a class, interface, enum,and annotation.

// package statement

package structureofjavasourcecodefile.structureofclass;
// package statement must be first statement in java source code file
// you could not define multiple package statement in one java source code file

//package naming conventions
// package names should all be in lowercase
// Package names follow the rules defined for valid identifiers in Java
// The hierarchy of classes and interfaces defined in packages must match the hierarchy of  the  directories
// in  which  these  classes  and  interfaces  are  defined  in  the  code.
// Example: this file must reside /path/to/projectfolder/structureofjavasourcecodefile/structureofclass


// com.oracle.javacert
// package naming convention
// com org edu gov = represent commercial organisation educational government
// oracle = organisation name
// javacert = project name

// import statement

// import statement is not embed imported class into your java source code file. Its not increase size of your file.
// import statement must follow package statement but precede class declaration
// you could not import classes from default package
// classes in default package that there is not any package statement inside java source code file
// look src/ClassInsideDefaultPackage.java

// import other classes in your java source code file you should import fully qualified name

import structureofjavasourcecodefile.company.demo.ClassUsedBySampleClass;
import structureofjavasourcecodefile.structureofclass.interfaces.InterfaceA;
import structureofjavasourcecodefile.structureofclass.interfaces.InterfaceB;
// keep in mind not  import company.demo.ClassUsedBySampleClass;
// only fully qualified name of class
// you cannot import classes with same name from different packages
import java.sql.Date;
// import java.util.Date; // uncomment this line will cause compile error


// you can import all classes inside package with below statement
// this statement allow you to use all classes under baseclasses package: BaseClassA and BaseClassB
// but be carefull  import statement doesn’t import the whole package tree. It is not import subpackages of baseclasses package: BaseClassC
import structureofjavasourcecodefile.structureofclass.baseclasses.*;

// below import statement is redundant. Java.lang package automatically imported all classes
import java.lang.*;

// you can import class with two ways
// 1. import one class
// package.subpackage.Classname;
// 2. import all classes inside packcage
// import package.subpackage.*;
// other syntaxes are wrong, such as : import package.subpackage; import package.subpackage*;


// static import
//You can import an individual static member of a class or all its static members by using the import static statement.
import static structureofjavasourcecodefile.structureofclass.staticimport.ClassContainsStaticMembers.name; // this statement allow you use name field
import static structureofjavasourcecodefile.structureofclass.staticimport.ClassContainsStaticMembers.*; // this statement allow you use name field

// comments can appear any where you want

// Javadoc comments Javadoc comments are special comments that start with /** and end with */ in a Java source file.
// These comments are processed by Javadoc, a JDK tool, to generateAPI documentation for your Java source code files/

/**
 * it's a javadoc
 */


// its class declaration
// The declaration of a class is composed of the following parts:
// Access modifiers | Non-access modifiers | class keyword | Class name | name of base class if class extends another class | implemented interfaces | class body inside curly bracket
// Main parts of class declaration are class keyword ClassName and class body {}
/*    class SampleClass{
 *
 *    }
 */
// keep in mind: all keywords in java are case sensitive.
// class can extends only one base class but can implements multiple interfaces same time
// reason of extending one class is diamon problem. Just google it "diamond problem in java"
final class SampleClass extends BaseClassA implements InterfaceA, InterfaceB {
//    A class is a design used to specify the attributes and behavior of an object.
//    The attributes of an object are implemented using variables, and the behavior is implementedusing methods.
// The  same design can be used to create multiple java objects - instances, just as the Java Virtual Machine(JVM) uses a class to create its objects.

    private ClassUsedBySampleClass classUsedBySampleClass;
    // private ClassInsideDefaultPackage classInsideDefaultPackage; // will cause compile error

    {
        name = "we did not declared this variable in this class we use import static statement";
        showName(); // this method is belong ClassContainsStaticMembers but using import static statement we can access all static members of mentioned class
    }

    // there is 2 type of comments in java:
    /* 1.multi line comments
    * The usage of * isn’t mandatory for each line; it’s done for aesthetic reasons
    without asterisk it's acceptable.
    */
    // 2.end of line comments

//    String someWrongStringWithComment = "It's sample /* unacceptable
//                         comment*/ string";

    String someCorrectStringWithComment = "It's sample" + /* acceptable
                         comment*/ " string ";  // must concatenate strings

    String someCorrectStringWithComment1 =  /* acceptable */ "It's sample string";

}


// you can define multiple class and interface inside same java source code file

interface InterfaceD {

}

interface InterfaceE {

}

final class AnotherClass { // final is non-access modifiers just optional here
    // non-public class use another non-public class
    private ClassB sometype;
}

/* will raise compile error due public keyword

public final class AnotherClassB{

}

*/

// note that you can define only one public type in same java source code file and file name must same as class that marked public access modifiers
// its possible create multiple java type (class,interface) same java source code file but non of them has public access modifiers
// take a look org.structureofclass.SameplSourceCodeFileWithoutPublicType.java